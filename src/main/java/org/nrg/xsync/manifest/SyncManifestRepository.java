package org.nrg.xsync.manifest;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

/**
 * Created by Michael Hileman on 2016/07/07.
 */
@Repository
public class SyncManifestRepository extends AbstractHibernateDAO<XsyncProjectHistory> {
}
